﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Conversions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Сотрудники
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class EmployeesController
    : ControllerBase
{
    private readonly IRepository<Employee> _employeeRepository;
    private readonly IRepository<Role> _rolesRepository;

    public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository)
    {
        _employeeRepository = employeeRepository;
        _rolesRepository = rolesRepository;
    }
        
    /// <summary>
    /// Получить данные всех сотрудников
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
    {
        var employees = await _employeeRepository.GetAllAsync();

        var employeesModelList = employees.Select(x => 
            new EmployeeShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FullName = x.FullName,
            }).ToList();

        return employeesModelList;
    }
        
    /// <summary>
    /// Получить данные сотрудника по Id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
    {
        var result = await _employeeRepository.GetByIdAsync(id);

        if (!result.Status.IsSuccess)
            return NotFound();
            
        var employeeModel = RequestConversion.ToResponse(result.Value);

        return employeeModel;
    }

    /// <summary>
    /// Добавить нового сотрудника
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<EmployeeResponse>> CreateEmployee(AddEmployeeRequest request)
    {
        var roles = (await _rolesRepository.GetAllAsync()).ToList();
        var newEmployee = RequestConversion.ToModel(request, roles);
        
        var result = await _employeeRepository.CreateAsync(newEmployee);
        if (!result.IsSuccess)
            return BadRequest(result.Message);
        
        return Ok(newEmployee);
    }

    /// <summary>
    /// Обновить сотрудника
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<ActionResult> UpdateEmployee(UpdateEmployeeRequest request)
    {
        var roles = (await _rolesRepository.GetAllAsync()).ToList();
        var modifiedEmployee = RequestConversion.ToModel(request, roles);
        
        var status = await _employeeRepository.UpdateAsync(modifiedEmployee);
        if (!status.IsSuccess)
            return BadRequest(status.Message);

        return Ok(request);
    }

    /// <summary>
    /// Удалить сотрудника
    /// </summary>
    /// <param name="request"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete("{id:guid}")]
    public async Task<ActionResult> RemoveEmployee(Guid id)
    {
        var result = await _employeeRepository.RemoveByIdAsync(id);
        if (!result.IsSuccess)
            return BadRequest(result.Message);

        return Ok();
    }
}