﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models;

public class IdEntity
{
    public Guid Id { get; set; }
}

public class RoleItemResponse : IdEntity
{
    public string Name { get; set; }

    public string Description { get; set; }
}