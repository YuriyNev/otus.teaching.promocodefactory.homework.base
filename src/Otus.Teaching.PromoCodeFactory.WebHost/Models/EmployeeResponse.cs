﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models;

public abstract class EmployeeEntity
{
    public string FullName { get; set; }

    public string Email { get; set; }
    
    public int AppliedPromocodesCount { get; set; }
}

public class AddEmployeeRequest : EmployeeEntity
{
    public string FirstName { get; set; }
    
    public string LastName { get; set; }
    
    public List<IdEntity> Roles { get; set; }
}

public class UpdateEmployeeRequest : AddEmployeeRequest
{
    public Guid Id { get; set; }
} 


public class EmployeeResponse : EmployeeEntity
{
    public Guid Id { get; set; }
    
    public List<RoleItemResponse> Roles { get; set; }
}