using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Conversions;

public static class RequestConversion
{
    public static EmployeeResponse ToResponse(Employee employee)
    {
        var employeeModel = new EmployeeResponse
        {
            Id = employee.Id,
            Email = employee.Email,
            Roles = employee.Roles.Select(x => new RoleItemResponse()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description
            }).ToList(),
            FullName = employee.FullName,
            AppliedPromocodesCount = employee.AppliedPromocodesCount
        };
        return employeeModel;
    }

    public static Employee ToModel(AddEmployeeRequest request, IReadOnlyList<Role> roles)
    {
        var employee = ToEmployee(request, roles);
        employee.Id = Guid.NewGuid();
        return employee;
    }

    public static Employee ToModel(UpdateEmployeeRequest request, IReadOnlyList<Role> roles)
    {
        var employee = ToEmployee(request, roles);
        employee.Id = request.Id;
        return employee;
    }

    private static Employee ToEmployee(AddEmployeeRequest request, IReadOnlyList<Role> roles)
    {
        var modifiedEmployee = new Employee
        {
            Email = request.Email,
            FirstName = request.FirstName,
            LastName = request.LastName,
            AppliedPromocodesCount = request.AppliedPromocodesCount,
            Roles = roles
                .Where(x => request.Roles.Any(r => r.Id == x.Id))
                .ToList(),
        };

        if (request.Roles.Count != modifiedEmployee.Roles.Count)
            throw new Exception("Unknown role id!");

        return modifiedEmployee;
    }
}