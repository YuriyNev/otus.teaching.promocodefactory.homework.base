﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class InMemoryRepository<T>
    : IRepository<T>
    where T: BaseEntity
{
    protected List<T> Data { get; set; }

    public InMemoryRepository(IEnumerable<T> data)
    {
        Data = data.ToList();
    }
        
    public Task<IEnumerable<T>> GetAllAsync()
    {
        return Task.FromResult((IEnumerable<T>)Data);
    }

    public Task<WithStatus<T>> GetByIdAsync(Guid id)
    {
        var element = Data.FirstOrDefault(x => x.Id == id);
        var status = new WithStatus<T>();
        if (element == null)
        {
            status.Status = ReportFactory.NotExistsStatus;
        }
        else
        {
            status.Status = ReportFactory.OkStatus;
            status.Value = element;            
        }
        
        return Task.FromResult(status);
    }

    public Task<OperationStatus> CreateAsync(T element)
    {
        if (element == null) throw new ArgumentNullException(nameof(element));
            
        if (Data.Any(x => x.Id == element.Id))
            return Task.FromResult(ReportFactory.NotExistsStatus);
            
        Data.Add(element);

        return Task.FromResult(ReportFactory.OkStatus);
    }

    public Task<OperationStatus> UpdateAsync(T element)
    {
        if (element == null) throw new ArgumentNullException(nameof(element));

        for (var i = 0; i < Data.Count; i++)
            if (Data[i].Id == element.Id)
            {
                Data[i] = element;
                return Task.FromResult(ReportFactory.OkStatus);
            }

        return Task.FromResult(ReportFactory.NotExistsStatus);
    }

    public Task<OperationStatus> RemoveByIdAsync(Guid id)
    {
        var element = Data.FirstOrDefault(x => x.Id == id);
        if (element == null) 
            return Task.FromResult(ReportFactory.NotExistsStatus);
            
        if (!Data.Remove(element))
            return Task.FromResult(ReportFactory.CannotRemoveStatus);
            
        return Task.FromResult(ReportFactory.OkStatus);
    }
}