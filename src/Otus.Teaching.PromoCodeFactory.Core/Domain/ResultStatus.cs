namespace Otus.Teaching.PromoCodeFactory.Core.Domain;

public class WithStatus<T>
{
    public T Value { get; set; }
    public OperationStatus Status { get; set; }
}

public class OperationStatus
{
    public bool IsSuccess { get; }
    public string Message { get; }
    
    public OperationStatus(string message, bool isSuccess)
    {
        Message = message;
        IsSuccess = isSuccess;
    }
}

public static class ReportFactory
{
    public static OperationStatus OkStatus => new(string.Empty, true);
    public static OperationStatus AlreadyExistsStatus => new ("Cannot add element because already exists!", false);
    public static OperationStatus NotExistsStatus => new ("Element is not exist!", false);
    public static OperationStatus CannotRemoveStatus => new ("Cannot remove!", false);
}