﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<WithStatus<T>> GetByIdAsync(Guid id);

        Task<OperationStatus> CreateAsync(T element);

        Task<OperationStatus> UpdateAsync(T element);

        Task<OperationStatus> RemoveByIdAsync(Guid id);
    }
}